package cl.amisoft.causaapp.service;

import cl.amisoft.causaapp.vo.CausaVo;
import java.util.List;

public interface CausaService {

  CausaVo obtener(Long id);

  List<CausaVo> listar();

  CausaVo agregar(CausaVo causa);

  CausaVo eliminar(Long causaId);

  CausaVo editar(Long causaId, CausaVo causa);
}
