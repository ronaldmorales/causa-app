package cl.amisoft.causaapp.service.impl;

import cl.amisoft.causaapp.client.TramiteCausaClient;
import cl.amisoft.causaapp.dao.CausaDao;
import cl.amisoft.causaapp.model.Causa;
import cl.amisoft.causaapp.service.CausaService;
import cl.amisoft.causaapp.vo.CausaVo;
import cl.amisoft.causaapp.vo.TramiteCausaVo;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CausaServiceImpl implements CausaService {

    private final CausaDao causaDao;
    private final TramiteCausaClient tramiteCausaClient;

    @Autowired
    public CausaServiceImpl(CausaDao causaDao, TramiteCausaClient tramiteCausaClient) {
        this.causaDao = causaDao;
        this.tramiteCausaClient = tramiteCausaClient;
    }

    @Override
    public CausaVo obtener(Long id) {
        Optional<Causa> causa = causaDao.obtenerCausa(id);
        if (causa.isPresent()) {
            List<TramiteCausaVo> tramitesCausa = tramiteCausaClient.obtenerTodosPorCausa(causa.get().getId());
            return new CausaVo.Builder()
                    .id(causa.get().getId())
                    .nombre(causa.get().getNombre())
                    .fechaCreacion(causa.get().getFechaCreacion())
                    .tramites(tramitesCausa)
                    .build();
        }
        return new CausaVo();
    }

  @Override
  public List<CausaVo> listar() {
    return causaDao.listar().stream()
        .map(c -> new CausaVo.Builder()
            .id(c.getId())
            .nombre(c.getNombre())
            .fechaCreacion(c.getFechaCreacion())
            .build())
        .collect(Collectors.toList());
  }

  @Transactional
  @Override
  public CausaVo agregar(CausaVo causaVo) {
    Causa causa = causaDao.agregar(new Causa.Builder()
        .id(causaVo.getId())
        .nombreTramite(causaVo.getNombre())
        .fechaCreacion(LocalDateTime.now())
        .build());
    return new CausaVo.Builder()
        .id(causa.getId())
        .nombre(causa.getNombre())
        .build();
  }

  @Override
  @Transactional
  public CausaVo eliminar(Long causaId) {
      Optional<Causa> causa = causaDao.obtenerCausa(causaId);
      if (causa.isPresent()) {
        Causa causa1 = causaDao.eliminar(causa.get());
        return new CausaVo.Builder()
            .id(causa.get().getId())
            .nombre(causa.get().getNombre())
            .fechaCreacion(causa.get().getFechaCreacion())
            .build();
      }
      return null;
  }

  @Override
  @Transactional
  public CausaVo editar(Long causaId, CausaVo causaVo) {
      Causa causa =  causaDao.editar(new Causa.Builder()
      .id(causaVo.getId())
      .nombreTramite(causaVo.getNombre())
      .fechaCreacion(LocalDateTime.now())
      .build());
      return new CausaVo.Builder()
          .id(causa.getId())
          .nombre(causa.getNombre())
          .fechaCreacion(causa.getFechaCreacion())
          .build();
  }
}